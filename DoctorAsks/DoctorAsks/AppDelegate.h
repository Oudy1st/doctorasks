//
//  AppDelegate.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/27/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

