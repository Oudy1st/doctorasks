//
//  QuestionListViewController.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/31/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "QuestionListViewController.h"

#import "QuestionInfo.h"

@interface QuestionListViewController ()

@end

@implementation QuestionListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    displayArray = [AppSession session].pQuestionArray;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return displayArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    UILabel *lblTH = (UILabel*)[cell viewWithTag:1];
    UILabel *lblMM = (UILabel*)[cell viewWithTag:2];
    
    
    QuestionInfo *info = [displayArray objectAtIndex:indexPath.row];
    
    lblTH.text = info.pTxtTh;
    lblMM.text = info.pTxtMM;
    
    lblTH.font = [FontHelper FontTHWithSize:lblTH.font.pointSize];
    lblMM.font = [FontHelper FontMMWithSize:lblMM.font.pointSize];
    
    
    UIView *view = (UIView*)[cell viewWithTag:3];
    
    if (cell.layer.cornerRadius == 0) {
        view.layer.cornerRadius = 5.0;
        view.layer.borderWidth = 1.0;
        view.layer.borderColor = [ColorHelper appColor:AppColor_yellowdark].CGColor;
    }
    
    if (indexPath.row %2 == 1) {
        view.backgroundColor = [ColorHelper appColor:AppColor_yellowdark withAlpha:0.2];
    }
    else
    {
        view.backgroundColor = [UIColor clearColor];
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    QuestionInfo *info = [displayArray objectAtIndex:indexPath.row];
    
    [AppSession session].pQuestionInfo = info;
    
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"questionVC"];
    
    [self.navigationController pushViewController:vc animated:YES];
}






-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    [_txtSearch resignFirstResponder];
}



- (IBAction)txtChanged:(id)sender 
{
    NSString *searchText = _txtSearch.text;
    
    if (searchText.length == 0) {
        displayArray = [AppSession session].pQuestionArray;
    }
    else
    {
        NSMutableArray *tempArray = [[NSMutableArray alloc]init];
        NSArray *qArray = [NSArray arrayWithArray:[AppSession session].pQuestionArray];
        for (QuestionInfo *info in qArray) {
            if ([info.pTxtTh containsString:searchText]) {
                [tempArray addObject:info];
            }
        }
        
        displayArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        qArray = nil;
    }
    
    [_tableViewMain reloadData];
    
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


@end
