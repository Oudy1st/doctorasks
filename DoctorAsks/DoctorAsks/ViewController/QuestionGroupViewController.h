//
//  QuestionGroupViewController.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/31/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionGroupViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    NSArray *displayArray;
}


@property (weak, nonatomic) IBOutlet UITableView *tableViewMain;
@property (weak, nonatomic) IBOutlet CustomSearchBar *txtSearch;

- (IBAction)txtChanged:(id)sender;
@end
