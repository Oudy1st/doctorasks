//
//  QuestionViewController.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/31/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "QuestionViewController.h"

#import "AnswerInfo.h"
#import "SoundPlayer.h"

@interface QuestionViewController ()

@end

@implementation QuestionViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    displayArray = [AppSession session].pQuestionInfo.pAnswerArray;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if([AppSession session].pQuestionInfo.pSoundExist)
    {
        [[SoundPlayer player]playSound:[AppSession session].pQuestionInfo.pSoundMM];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[SoundPlayer player] stop];
    
    [super viewWillDisappear:animated];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    }
    else
    {
        return displayArray.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QuestionCell" forIndexPath:indexPath];
        [self configureQuestionCell:cell atIndexPath:indexPath];
        return cell;
    }
    else
    {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        [self configureAnswerCell:cell atIndexPath:indexPath];
        return cell;
    }
    
}

-(void)configureQuestionCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    // Configure the cell...
    UILabel *lblTH = (UILabel*)[cell viewWithTag:1];
    UILabel *lblMM = (UILabel*)[cell viewWithTag:2];
    
    
    QuestionInfo *info = [AppSession session].pQuestionInfo;
    
    lblTH.text = info.pTxtTh;
    lblMM.text = info.pTxtMM;
    
    lblTH.font = [FontHelper FontTHWithSize:lblTH.font.pointSize];
    lblMM.font = [FontHelper FontMMWithSize:lblMM.font.pointSize];

    
    UIView *view = (UIView*)[cell viewWithTag:3];

    if (cell.layer.cornerRadius == 0) {
        view.layer.cornerRadius = 5.0;
        view.layer.borderWidth = 3.0;
        view.layer.borderColor = [ColorHelper appColor:AppColor_green].CGColor;
    }
    view.backgroundColor = [ColorHelper appColor:AppColor_yellowdark withAlpha:1.0];
    
}

-(void)configureAnswerCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    // Configure the cell...
    UILabel *lblTH = (UILabel*)[cell viewWithTag:1];
    UILabel *lblMM = (UILabel*)[cell viewWithTag:2];
    
    
    AnswerInfo *info = [displayArray objectAtIndex:indexPath.row];
    
    lblTH.text = info.pTxtTh;
    lblMM.text = info.pTxtMM;
    
    lblTH.font = [FontHelper FontTHWithSize:lblTH.font.pointSize];
    lblMM.font = [FontHelper FontMMWithSize:lblMM.font.pointSize];
    
    
    UIView *view = (UIView*)[cell viewWithTag:3];
    
    if (cell.layer.cornerRadius == 0) {
        view.layer.cornerRadius = 5.0;
        view.layer.borderWidth = 1.0;
        view.layer.borderColor = [ColorHelper appColor:AppColor_yellowdark].CGColor;
    }
    
    if (indexPath.row %2 == 1) {
        view.backgroundColor = [ColorHelper appColor:AppColor_yellowdark withAlpha:0.2];
    }
    else
    {
        view.backgroundColor = [UIColor clearColor];
    }
    
    
    UIImageView *imageView = (UIImageView*)[cell viewWithTag:4];
    
    [imageView setHighlighted:!info.pSoundExist];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        if ([AppSession session].pQuestionInfo.pSoundExist) {
            
            [[SoundPlayer player] playSound:[AppSession session].pQuestionInfo.pSoundMM];
        }
    }
    else
    {
        if ([[displayArray objectAtIndex:indexPath.row] pSoundExist]) {
            [[SoundPlayer player] playSound:[[displayArray objectAtIndex:indexPath.row] pSoundMM]];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [self heightForQuestionCellAtIndexPath:indexPath];
    }
    else
    {
        return [self heightForAnswerCellAtIndexPath:indexPath];
    }
}

- (CGFloat)heightForQuestionCellAtIndexPath:(NSIndexPath *)indexPath {
    static UITableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableViewMain dequeueReusableCellWithIdentifier:@"QuestionCell"];
    });
    
    [self configureQuestionCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)heightForAnswerCellAtIndexPath:(NSIndexPath *)indexPath {
    static UITableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableViewMain dequeueReusableCellWithIdentifier:@"Cell"];
    });
    
    [self configureAnswerCell:sizingCell atIndexPath:indexPath];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1.0f; // Add 1.0f for the cell separator height
}

@end
