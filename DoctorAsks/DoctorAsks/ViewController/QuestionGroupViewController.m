//
//  QuestionGroupViewController.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/31/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "QuestionGroupViewController.h"

#import "QuestionGroupInfo.h"

@interface QuestionGroupViewController ()


@end

@implementation QuestionGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[AppSession session] load];
    
    displayArray = [AppSession session].pQuestionGroupArray;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return displayArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    UILabel *lblTH = (UILabel*)[cell viewWithTag:1];
    UILabel *lblMM = (UILabel*)[cell viewWithTag:2];
    
    
    QuestionGroupInfo *info = [displayArray objectAtIndex:indexPath.row];
    
    lblTH.text = info.pTxtTh;
    lblMM.text = info.pTxtMM;
    
    lblTH.font = [FontHelper FontTHWithSize:lblTH.font.pointSize];
    lblMM.font = [FontHelper FontMMWithSize:lblMM.font.pointSize];
    
    
    UIView *view = (UIView*)[cell viewWithTag:3];
    
    if (cell.layer.cornerRadius == 0) {
        view.layer.cornerRadius = 5.0;
        view.layer.borderWidth = 1.0;
        view.layer.borderColor = [ColorHelper appColor:AppColor_yellowdark].CGColor;
    }
    
    if (indexPath.row %2 == 1) {
        view.backgroundColor = [ColorHelper appColor:AppColor_yellowdark withAlpha:0.2];
    }
    else
    {
        view.backgroundColor = [UIColor clearColor];
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    QuestionGroupInfo *info = [displayArray objectAtIndex:indexPath.row];
    
    [AppSession session].pQuestionArray = info.pQuestionArray;
    
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"questionListVC"];
    
    [self.navigationController pushViewController:vc animated:YES];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
    [_txtSearch resignFirstResponder];
}



- (IBAction)txtChanged:(id)sender {
    
    NSString *searchText = _txtSearch.text;
    
    if (searchText.length == 0) {
        displayArray = [AppSession session].pQuestionGroupArray;
    }
    else
    {
        NSMutableArray *tempArray = [[NSMutableArray alloc]init];
        NSArray *qArray = [NSArray arrayWithArray:[AppSession session].pQuestionGroupArray];
        for (QuestionGroupInfo *info in qArray) {
            if ([info.pTxtTh containsString:searchText]) {
                [tempArray addObject:info];
            }
        }
        
        displayArray = [NSArray arrayWithArray:tempArray];
        tempArray = nil;
        qArray = nil;
    }
    
    [_tableViewMain reloadData];
}




-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
@end
