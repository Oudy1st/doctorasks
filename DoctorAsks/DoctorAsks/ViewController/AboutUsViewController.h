//
//  AboutUsViewController.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 4/2/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewMain;
@end
