//
//  CommendViewController.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 4/2/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "CommendViewController.h"

@interface CommendViewController ()

@end

@implementation CommendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnFacebookDidTap:(id)sender {
    
    NSURL *urlApp = [NSURL URLWithString:@"fb://profile/774129062660398"];
    [[UIApplication sharedApplication] openURL:urlApp];
    
    
    urlApp = [NSURL URLWithString:@"https://www.facebook.com/DoctorAsks"];
    [[UIApplication sharedApplication] openURL:urlApp];
    
}
@end
