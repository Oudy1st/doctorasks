//
//  QuestionViewController.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/31/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *displayArray;
}


@property (weak, nonatomic) IBOutlet UITableView *tableViewMain;


@end
