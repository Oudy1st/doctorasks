//
//  CustomNavViewController.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 4/2/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavViewController : UINavigationController<UINavigationControllerDelegate>


@property (nonatomic,assign) BOOL pIsHideBackButton;

+ (UIBarButtonItem*)buttonWithTarget:(id)target action:(SEL)action andImage:(UIImage*)image;
+ (UIBarButtonItem*)buttonWithTarget:(id)target action:(SEL)action andStyle:(UIBarButtonSystemItem)style;

@end
