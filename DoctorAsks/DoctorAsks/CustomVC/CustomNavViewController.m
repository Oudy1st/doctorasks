//
//  CustomNavViewController.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 4/2/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "CustomNavViewController.h"

@interface CustomNavViewController ()
{
    UIImage *backImage;
    
    UIView *titleView;
}

@end

@implementation CustomNavViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationBar setBarStyle:UIBarStyleDefault];
    [self.navigationBar setTranslucent:NO];
    
    
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg_header_green_02"] forBarMetrics:UIBarMetricsDefault];
    
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     @{NSForegroundColorAttributeName: [UIColor whiteColor],
       //              UITextAttributeTextShadowColor: [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8],
       //              UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, -1)],
       NSFontAttributeName: [UIFont systemFontOfSize:20],
       }];
    
    
    self.delegate = self;

    

    
    
}






-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{

    
    
    
    if( self.viewControllers.count > 1)
    {
        viewController.navigationItem.leftBarButtonItems = @[[self backArrowButtonWithTarget:self action:@selector(popViewControllerAnimated:)],[self getTitleView]];
    }
    else
    {
        
        viewController.navigationItem.leftBarButtonItem = [self getTitleView];
    }
    
    if (_pIsHideBackButton) {
        viewController.navigationItem.leftBarButtonItem = nil;
        viewController.navigationItem.hidesBackButton = YES;
        _pIsHideBackButton = NO;
    }
}


-(UIViewController *)popViewControllerAnimated:(BOOL)animated
{
    return [super popViewControllerAnimated:YES];
}


- (UIBarButtonItem*)getTitleView
{
    if (!titleView) {
        CGRect frame = CGRectMake(0,0, 200, 30);
        titleView = [[UIView alloc]initWithFrame:frame];
        titleView.backgroundColor = [UIColor redColor];
        titleView.backgroundColor = [UIColor clearColor];
        
        frame = CGRectMake(0, 0, 30, 30);
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
        imageView.image = [UIImage imageNamed:@"ic_logo"];
        imageView.backgroundColor = [UIColor clearColor];
        
        frame = CGRectMake(40, 0, 10, 30);
        UILabel *label = [[UILabel alloc]initWithFrame:frame];
        label.text =@"DoctorAsks Myanmar";
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        frame.size.width =  [label sizeThatFits:frame.size].width;
        
        label.frame = frame;
        label.textAlignment = NSTextAlignmentCenter;
        
        frame.size.width +=frame.origin.x;
        frame.origin.x = 0;
        frame.size.height = 30;
        titleView.frame = frame;
        
        [titleView addSubview:imageView];
        [titleView addSubview:label];
    }
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:titleView];
    
    
    return customBarItem;
    
}


- (UIBarButtonItem*)backArrowButtonWithTarget:(id)target action:(SEL)action
{
    //UIImage *buttonImage = [UIImage imageNamed:COLOR_ICON_BACK];
    if (!backImage) {
        backImage = [UIImage imageNamed:@"ic_back"];
    }
    UIImage *buttonImage = backImage;
    
    //create the button and assign the image
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    
    //set the frame of the button to the size of the image (see note below)
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    
    button.contentEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    //create a UIBarButtonItem with the button as a custom view
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];

    
    return customBarItem;
}




+ (UIBarButtonItem*)buttonWithTarget:(id)target action:(SEL)action andImage:(UIImage*)image
{
    //UIImage *buttonImage = [UIImage imageNamed:COLOR_ICON_BACK];
    UIImage *buttonImage = image;
    
    //create the button and assign the image
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    
    //set the frame of the button to the size of the image (see note below)
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    //create a UIBarButtonItem with the button as a custom view
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    return customBarItem;
}

+ (UIBarButtonItem*)buttonWithTarget:(id)target action:(SEL)action andStyle:(UIBarButtonSystemItem)style
{
    
    
    //create a UIBarButtonItem with the button as a custom view
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:style target:target action:action];
    return customBarItem;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
