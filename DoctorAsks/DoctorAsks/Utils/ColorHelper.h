//
//  ColorHelper.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 4/1/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    AppColor_itemselected,
    AppColor_survey_color,
    AppColor_sign_out_color,
    AppColor_login_hover,
    AppColor_yellowdark,
    AppColor_green,
    AppColor_white,
    AppColor_cell_highlight,
} AppColor;


@interface ColorHelper : NSObject


+(UIColor*)appColor:(AppColor)appcolor;
+(UIColor*)appColor:(AppColor)appcolor withAlpha:(CGFloat)alpha;
+(UIColor*)colorWithHexString:(NSString*)hex withAlpha:(CGFloat)alpha;
+(UIColor*)colorWithAlphaHexString:(NSString*)hex;


+ (UIImage *)imageWithColor:(UIColor *)color;
@end
