//
//  UIBorderTextField.m
//  RamaApp
//
//  Created by EGA_GADD on 9/11/2557 BE.
//  Copyright (c) 2557 EGA. All rights reserved.
//

#import "UIBorderTextField.h"

@implementation UIBorderTextField

-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.layer.cornerRadius = 3.0;
    self.layer.borderColor = (__bridge CGColorRef)([UIColor colorWithRed:90/255.0f green:89/255.0f blue:92/255.0f alpha:0.3f]);
    self.layer.borderWidth = 1.0f;
}

- (void)drawPlaceholderInRect:(CGRect)rect{
    if (self.placeholder)
    {
        
        // color of placeholder text
        UIColor *placeHolderTextColor = [UIColor darkGrayColor];
        
        CGSize drawSize = [self.placeholder sizeWithAttributes:[NSDictionary dictionaryWithObject:self.font forKey:NSFontAttributeName]];
        CGRect drawRect = rect;
        
        // verticially align text
        drawRect.origin.y = (rect.size.height - drawSize.height) * 0.5;
        
        // set alignment
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.alignment = self.textAlignment;
        
        // dictionary of attributes, font, paragraphstyle, and color
        NSDictionary *drawAttributes = @{NSFontAttributeName: self.font,
                                         NSParagraphStyleAttributeName : paragraphStyle,
                                         NSForegroundColorAttributeName : placeHolderTextColor};
        
        
        // draw
        [self.placeholder drawInRect:drawRect withAttributes:drawAttributes];
        
    }
}


-(void)addLeftText:(NSString*)text
{
    [self.leftView removeFromSuperview];
    
    UIView *view = [[UIView alloc]initWithFrame:(CGRect){{0,0},{90,self.frame.size.height}}];
    view.backgroundColor = [UIColor clearColor];
    
    UILabel *label = [[UILabel alloc]initWithFrame:(CGRect){{10,0},{70,self.frame.size.height}}];
    label.text = text;
    [label setTextColor:[UIColor grayColor]];
    
    [view addSubview:label];
    
    self.leftView = view;
    self.leftViewMode = UITextFieldViewModeAlways;
}
-(void)addRightButton:(UIButton *)button
{
    [self.rightView removeFromSuperview];
    
    UIView *view = [[UIView alloc]initWithFrame:button.bounds];
    view.backgroundColor = [UIColor clearColor];
    
    
    [view addSubview:button];
    self.rightView = view;
    self.rightViewMode = UITextFieldViewModeAlways;
}

-(void)addLeftItem:(UIView *)item
{
    [self.leftView removeFromSuperview];
    
    
    self.leftView = item;
    self.leftViewMode = UITextFieldViewModeAlways;
    
}
-(void)addLeftButton:(UIButton *)button
{
    [self.leftView removeFromSuperview];
    
    UIView *view = [[UIView alloc]initWithFrame:button.bounds];
    view.backgroundColor = [UIColor clearColor];
    
    
    [view addSubview:button];
    self.leftView = view;
    self.leftViewMode = UITextFieldViewModeAlways;
}

-(void)setBorderColor:(UIColor*)color andWidth:(CGFloat)width
{
    
    self.layer.borderColor = [color CGColor];
    self.layer.borderWidth = 3.0f;
}


@end
