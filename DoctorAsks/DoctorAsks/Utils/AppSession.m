//
//  AppSession.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/30/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "AppSession.h"

#import "QuestionGroupInfo.h"
#import "QuestionInfo.h"
#import "QALinkInfo.h"
#import "AnswerInfo.h"


#import "FileHelper.h"

@implementation AppSession
static AppSession *_appSession;


+(AppSession *)session
{
    if (!_appSession) {
        _appSession = [[AppSession alloc]init];
        
        
    }
    
    return _appSession;
}


-(BOOL)load
{
    _pQuestionGroupArray = nil;
    
    NSString *fileName = @"Question_Group";
    
    
    
    NSDictionary *dic = [FileHelper loadJsonTextFile:fileName];
    NSArray *array = [dic objectForKey:@"Question_Group"];
    if (!array) {
        return NO;
    }
    
    
    
    _pQuestionGroupArray = [QuestionGroupInfo initWithArray:array];
    
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pID"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    _pQuestionGroupArray = [_pQuestionGroupArray sortedArrayUsingDescriptors:sortDescriptors];
    
    
    
    NSArray *questionArray = [self loadQuestion];
    NSArray *qaLinkArray = [self loadQALink];
    NSArray *answerArray = [self loadAnswer];
    for (QuestionGroupInfo *qgInfo in _pQuestionGroupArray) {
        
        NSMutableArray *qArray = [[NSMutableArray alloc]init];
        for (QuestionInfo *qInfo in questionArray) {
            
            if (qgInfo.pID == qInfo.pGroupID) {
                [qArray addObject:qInfo];
                
                NSMutableArray *aArray = [[NSMutableArray alloc]init];
                
                for (QALinkInfo *qaInfo in qaLinkArray) {
                    if (qaInfo.pQuestionID == qInfo.pID) {
                        
                        for (AnswerInfo *aInfo in answerArray) {
                            if (qaInfo.pAnswerID == aInfo.pID) {
                                [aArray addObject:aInfo];
                                break;
                            }
                        }
                        
                    }
                }
                
                qInfo.pAnswerArray = [NSArray arrayWithArray:aArray];
                
            }
            
            qgInfo.pQuestionArray = [NSArray arrayWithArray:qArray];
        }
        
    }
    
    
    
    
    
    
    return YES;
}


-(NSArray*)loadQuestion
{
    NSArray *result = nil;
    
    NSString *fileName = @"Question_List";
    
    
    
    NSDictionary *dic = [FileHelper loadJsonTextFile:fileName];
    NSArray *array = [dic objectForKey:@"Question_List"];
    if (!array) {
        return nil;
    }
    
    
    array = [QuestionInfo initWithArray:array];
    
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pID"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    array = [array sortedArrayUsingDescriptors:sortDescriptors];
    
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    NSInteger previosID = 0;
    for (int i = 0 ; i< array.count; i++) {
        QuestionInfo *info = [array objectAtIndex:i];
        
        if (info.pID != previosID) {
            [tempArray addObject:info];
            previosID = info.pID;
        }
        
    }
    
    result = [NSArray arrayWithArray:tempArray];
    
    
    
    return result;
}


-(NSArray*)loadQALink
{
    NSArray *result = nil;
    
    NSString *fileName = @"QA_List";
    
    
    
    NSDictionary *dic = [FileHelper loadJsonTextFile:fileName];
    NSArray *array = [dic objectForKey:@"QA_List"];
    if (!array) {
        return nil;
    }
    
    
    array = [QALinkInfo initWithArray:array];
    
    
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pID"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    array = [array sortedArrayUsingDescriptors:sortDescriptors];
    
    
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    NSInteger previosID = 0;
    for (int i = 0 ; i< array.count; i++) {
        QALinkInfo *info = [array objectAtIndex:i];
        
        if (info.pID != previosID) {
            [tempArray addObject:info];
            previosID = info.pID;
        }
        
    }
    
    result = [NSArray arrayWithArray:tempArray];
    
    
    
    
    return result;
}


-(NSArray*)loadAnswer
{
    NSArray *result = nil;
    
    NSString *fileName = @"Answer_List";
    
    
    
    NSDictionary *dic = [FileHelper loadJsonTextFile:fileName];
    NSArray *array = [dic objectForKey:@"Answer_List"];
    if (!array) {
        return nil;
    }
    
    array = [AnswerInfo initWithArray:array];
    
    
    
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pID"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    array = [array sortedArrayUsingDescriptors:sortDescriptors];
    
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    NSInteger previosID = 0;
    for (int i = 0 ; i< array.count; i++) {
        AnswerInfo *info = [array objectAtIndex:i];
        
        if (info.pID != previosID) {
            [tempArray addObject:info];
            previosID = info.pID;
        }
        
    }
    
    result = [NSArray arrayWithArray:tempArray];
    
    
    
    
    return result;
}



@end
