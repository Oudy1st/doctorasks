//
//  CustomSearchBar.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 4/2/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "UIBorderTextField.h"

@interface CustomSearchBar : UIBorderTextField

@end
