//
//  SoundPlayer.h
//  Greetee
//
//  Created by Oudy on 9/29/2557 BE.
//  Copyright (c) 2557 zoso. All rights reserved.
//

#import <Foundation/Foundation.h>


#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface SoundPlayer : NSObject<AVAudioPlayerDelegate>
{
    AVAudioPlayer *player;
}

+(SoundPlayer*)player;

-(void)playSound:(NSString*)filename;
-(void)stop;

@end
