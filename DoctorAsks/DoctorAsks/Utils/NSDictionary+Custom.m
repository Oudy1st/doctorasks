//
//  NSDictionary+Custom.m
//  RamaApp
//
//  Created by Vaetita Yusabye on 9/23/14.
//  Copyright (c) 2014 EGA. All rights reserved.
//

#import "NSDictionary+Custom.h"


@implementation NSDictionary (Custom)
-(id)objectNotNullForKey:(id)aKey{
    @try {
        return [self objectForKey:aKey];
    }
    @catch (NSException *exception) {
        return nil;
    }
}

-(NSString *)stringNotNullForKey:(id)aKey{
    @try {
        id obj = [self objectForKey:aKey];
        if ([obj isKindOfClass:[NSString class]]) {
            if (!obj) {
                return @"";
            } else if ([obj rangeOfString:@"null"].location != NSNotFound) {
                obj = nil;
                return @"";
            }else {
                return obj;
            }
        } else if ([obj isEqual:[NSNull null]]){
            obj = nil;
            return @"";
        } else {
            return obj;
        }
    }
    @catch (NSException *exception) {
        return @"";
    }
}


-(double)doubleForKey:(id)aKey{
    @try {
        return [[self objectForKey:aKey]doubleValue];
    }
    @catch (NSException *exception) {
        return 0;
    }
}

-(BOOL)boolForKey:(id)aKey{
    @try {
        return [[self objectForKey:aKey]boolValue];
    }
    @catch (NSException *exception) {
        return NO;
    }
}

-(int)intForKey:(id)aKey{
    @try {
        return [[self objectForKey:aKey]intValue];
    }
    @catch (NSException *exception) {
        return 0;
    }
}

-(long)longForKey:(id)aKey{
    @try {
        return [[self objectForKey:aKey]longValue];
    }
    @catch (NSException *exception) {
        return 0;
    }
}


-(void)setObjectIfNotNull:(id)value forKey:(NSString *)key{
    if (value) {
        if ([self isKindOfClass:[NSMutableDictionary class]]) {
            [((NSMutableDictionary *)self)setObject:value forKey:key];
        }
    }
}

+(id)dictionaryJSONWithString:(NSString *)string{
    @try {
        NSError *error = nil;
        return [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    }
    @catch (NSException *exception) {
        NSLog(@"Cannot parse string to dict: %@ | %@", exception.description, exception.userInfo);
        return nil;
    }
}

+(NSString *)stringJSONWithDictionary:(NSDictionary *)dictionary{
    @try {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:nil];
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    @catch (NSException *exception) {
        NSLog(@"Cannot parse dict to string: %@ | %@", exception.description, exception.userInfo);
        return @"";
    }
}

@end
