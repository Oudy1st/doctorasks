//
//  CustomSearchBar.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 4/2/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "CustomSearchBar.h"

@implementation CustomSearchBar


- (void)drawRect:(CGRect)rect {
    // Drawing code
    [super drawRect:rect];
    
    
    self.layer.cornerRadius = 5.0;
    self.layer.borderColor = [ColorHelper appColor:AppColor_green withAlpha:1].CGColor;
    self.layer.borderWidth = 1.0f;
    
    self.backgroundColor = [ColorHelper appColor:AppColor_yellowdark withAlpha:1];

    CGFloat padding = 5;
    CGRect frame =CGRectMake(padding*2, padding, rect.size.height - padding - padding, rect.size.height - padding - padding);
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
    imageView.image =[UIImage imageNamed:@"ico_search"];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, rect.size.height, rect.size.height)];
    [view addSubview:imageView];
    
    
    [self addLeftItem:view];


    [self setPlaceholder:@"Search..."];
}


@end
