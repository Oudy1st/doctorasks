//
//  AppSession.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/30/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuestionInfo.h"
@interface AppSession : NSObject

+(AppSession*)session;



-(BOOL)load;

@property (nonatomic,strong) NSArray *pQuestionGroupArray;
@property (nonatomic,strong) NSArray *pQuestionArray;
@property (nonatomic,strong) QuestionInfo *pQuestionInfo;

@end
