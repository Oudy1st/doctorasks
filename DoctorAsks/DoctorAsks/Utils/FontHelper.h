//
//  FontHelper.h
//  Horo
//
//  Created by Oudy on 10/2/2557 BE.
//  Copyright (c) 2557 zoso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FontHelper : NSObject

+(UIFont*)FontTHWithSize:(CGFloat)size;
+(UIFont*)FontMMWithSize:(CGFloat)size;

+(void)setFont:(UIFont*)font toView:(UIView*)label;

@end
