//
//  NSDictionary+Custom.h
//  RamaApp
//
//  Created by Vaetita Yusabye on 9/23/14.
//  Copyright (c) 2014 EGA. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSDictionary (Custom)
-(id)objectNotNullForKey:(id)aKey;
-(NSString *)stringNotNullForKey:(id)aKey;

-(double)doubleForKey:(id)aKey;
-(BOOL)boolForKey:(id)aKey;
-(int)intForKey:(id)aKey;
-(long)longForKey:(id)aKey;

-(void)setObjectIfNotNull:(id)value forKey:(NSString *)key;

+(id)dictionaryJSONWithString:(NSString *)string;
+(NSString *)stringJSONWithDictionary:(NSDictionary *)dictionary;
@end
