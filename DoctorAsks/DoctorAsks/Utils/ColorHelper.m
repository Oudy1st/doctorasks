//
//  ColorHelper.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 4/1/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "ColorHelper.h"

@implementation ColorHelper


+(UIColor*)appColor:(AppColor)appcolor
{
    return [ColorHelper appColor:appcolor withAlpha:1.0];
}

+(UIColor*)appColor:(AppColor)appcolor withAlpha:(CGFloat)alpha
{
    UIColor *color;
    
    switch (appcolor) {
        case AppColor_green:
            color = [ColorHelper colorWithHexString:@"30a052" withAlpha:alpha];
            break;
        case AppColor_itemselected:
            color = [ColorHelper colorWithHexString:@"EDEDED" withAlpha:alpha];
            break;
        case AppColor_login_hover:
            color = [ColorHelper colorWithHexString:@"E5F5FA" withAlpha:alpha];
            break;
        case AppColor_sign_out_color:
            color = [ColorHelper colorWithHexString:@"e84040" withAlpha:alpha];
            break;
        case AppColor_survey_color:
            color = [ColorHelper colorWithHexString:@"f7f2d7" withAlpha:alpha];
            break;
        case AppColor_white:
            color = [ColorHelper colorWithHexString:@"ffffff" withAlpha:alpha];
            break;
        case AppColor_yellowdark:
            color = [ColorHelper colorWithHexString:@"ffcc00" withAlpha:alpha];
            break;
        case AppColor_cell_highlight:
            color = [ColorHelper colorWithHexString:@"" withAlpha:alpha];
        default:
            color = [UIColor greenColor];
            break;
    }
    
    
    return color;
    
    //<color name="itemselected">#EDEDED</color>
    //<color name="survey_color">#f7f2d7</color>
    //<color name="sign_out_color">#e84040</color>
    //<color name="login_hover">#E5F5FA</color>
    //
    //<color name="yellowdark">#ffcc00</color>
    //<color name="green">#ff30a052</color>
    //<color name="white">#ffffff</color>
}


+(UIColor*)colorWithAlphaHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 8) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 8) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    NSString *alphaString  =[cString substringWithRange:range];
    
    range.location = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 6;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int a, r, g, b;
    [[NSScanner scannerWithString:alphaString] scanHexInt:&a];
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:(float) a];
}


+(UIColor*)colorWithHexString:(NSString*)hex withAlpha:(CGFloat)alpha
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:alpha];
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
@end
