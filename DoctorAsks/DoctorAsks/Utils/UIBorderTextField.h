//
//  UIBorderTextField.h
//  RamaApp
//
//  Created by EGA_GADD on 9/11/2557 BE.
//  Copyright (c) 2557 EGA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBorderTextField : UITextField

-(void)addLeftText:(NSString*)text;
-(void)addRightButton:(UIButton*)button;
-(void)addLeftItem:(UIView*)item;

@end
