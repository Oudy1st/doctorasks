//
//  SoundPlayer.m
//  Greetee
//
//  Created by Oudy on 9/29/2557 BE.
//  Copyright (c) 2557 zoso. All rights reserved.
//

#import "SoundPlayer.h"

@implementation SoundPlayer

static SoundPlayer *_player;

+(SoundPlayer *)player
{
    if (!_player) {
        _player = [[SoundPlayer alloc]init];
    }
    return _player;
}





-(void)playSound:(NSString*)filename;
{
    
    NSError *error;
    NSString *path;
    NSURL *soundUrl;
    
        path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath],filename];
        soundUrl = [NSURL fileURLWithPath:path];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:&error];
        
        if (error)
        {
            NSLog(@"Error in audioPlayer: %@",
                  [error localizedDescription]);
        } else {
            player.delegate = self;
        }
        [player setVolume:10];
        [player setNumberOfLoops:0];
        [player prepareToPlay];
    
    [player play];
}

-(void)stop
{
    [player stop];
}

@end
