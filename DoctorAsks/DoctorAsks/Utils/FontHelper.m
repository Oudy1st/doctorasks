//
//  FontHelper.m
//  Horo
//
//  Created by Oudy on 10/2/2557 BE.
//  Copyright (c) 2557 zoso. All rights reserved.
//

#import "FontHelper.h"

@implementation FontHelper



+(UIFont *)FontMMWithSize:(CGFloat)size
{
    
    return [UIFont fontWithName:@"Zawgyi-One" size:size];
}

+(UIFont *)FontTHWithSize:(CGFloat)size
{
    
    return [UIFont fontWithName:@"TH Sarabun New" size:size];
}

+(void)setFont:(UIFont *)font toView:(UIView *)label
{
    
    if ([label isKindOfClass:[UILabel class]]) {
        [(UILabel*)label setFont:font];
    }
    else if ([label isKindOfClass:[UIButton class]])
    {
        [[(UIButton*)label titleLabel] setFont:font];
        
    }
}


@end
