//
//  FileHelper.h
//  Greetee
//
//  Created by Oudy on 2/21/2557 BE.
//  Copyright (c) 2557 zoso. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    
    DocumentPathSource = 0,
    DocumentPathChat = 1
}DocumentPath;



@interface FileHelper : NSObject <NSCopying>

+ (NSString*)getPath:(DocumentPath)dPath WithName:(NSString*)name;
+ (BOOL)isExistWithPath:(DocumentPath)dPath andName:(NSString*)name;
+ (NSData*)dataWithPath:(DocumentPath)dPath andName:(NSString*)name;
+ (void)save:(NSData *)data withPath:(DocumentPath)dPath andName:(NSString*)name;
+ (void)deleteWithPath:(DocumentPath)dPath andName:(NSString*)name;

+ (void)deleteWithPath:(DocumentPath)dPath;

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

+(id)UnArchiveData:(NSData *)data;
+(NSData *)ArchiveData:(id)obj;

+(BOOL)isBundleExist:(NSString*)fileName;


+(NSData*)getDataFromURL:(NSString*)urlString;
+(void)preloadItemFromURL:(NSString*)urlString;


+(id)loadJsonTextFile:(NSString*)fileName;
@end
