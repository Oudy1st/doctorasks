//
//  FileHelper.m
//  Greetee
//
//  Created by Oudy on 2/21/2557 BE.
//  Copyright (c) 2557 zoso. All rights reserved.
//

#import "FileHelper.h"
#import <sys/xattr.h>

@implementation FileHelper

static NSString *kDataKey = @"FileData";

- (id)copyWithZone:(NSZone *)zone{
    return [[[self class]allocWithZone:zone]init];
}

+ (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}
+ (NSString *)applicationCacheDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+ (NSString *)documentpath:(DocumentPath)dPath
{
    NSString *result;
    switch (dPath) {
            
        case DocumentPathSource:
            
            result = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"Source"];
            break;
            
        case DocumentPathChat:
            result = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"Chat"];
            
        default:
            break;
    }
    
    if (![[NSFileManager defaultManager]fileExistsAtPath:result]) {
        NSError *error;
        [[NSFileManager defaultManager]createDirectoryAtPath:result withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    return result;
}

+ (NSString*)getPath:(DocumentPath)dPath WithName:(NSString*)name
{
    NSString *filePath = [self documentpath:dPath];
    
    filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", name]];
    return filePath;
}


+ (BOOL)isExistWithPath:(DocumentPath)dPath andName:(NSString*)name
{
    BOOL result;
    
    NSString* filePath = [self getPath:dPath WithName:name];
    
    result = ([[NSFileManager defaultManager] fileExistsAtPath:filePath]);
    
    return result;
}

+ (NSData*)dataWithPath:(DocumentPath)dPath andName:(NSString*)name
{
    name = [name stringByReplacingOccurrencesOfString:@"&" withString:@"_"];
    
    NSString* filePath = [self getPath:dPath WithName:name];
    
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    
    return data;
}

+ (void)save:(NSData *)data withPath:(DocumentPath)dPath andName:(NSString*)name
{
    
    if (data.length == 0) {
//        NSLog(@"no file");
        return;
    }
    
    
    NSString *filePath = [self documentpath:dPath];
    
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:filePath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
    }
    
    
    filePath = [self getPath:dPath WithName:name];
    BOOL result;

    result = [data writeToFile:filePath atomically:YES];
    
    if (result) {
        [self addSkipBackupAttributeToItemAtURL:[[NSURL alloc] initFileURLWithPath:filePath]];
    }
    
}
+ (void)deleteWithPath:(DocumentPath)dPath andName:(NSString*)name
{
    NSString* filePath = [self getPath:dPath WithName:name];
    
    
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
}
+ (void)deleteWithPath:(DocumentPath)dPath
{
    NSString* filePath = [self documentpath:dPath];
    
    NSError *error;
    NSArray *dirContents = [[NSFileManager defaultManager]
                            contentsOfDirectoryAtPath:filePath error:&error];
    
    for (NSString *path in dirContents) {
        
        filePath = [self getPath:dPath WithName:path];
        if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
        {
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
    }
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    // For iOS >= 5.1
        if(![[NSFileManager defaultManager] fileExistsAtPath: [URL path]]) return false;
        
        const char* filePath = [[URL path] fileSystemRepresentation];
        
        const char* attrName = "com.apple.MobileBackup";
        u_int8_t attrValue = 1;
        
        int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
        return result == 0;
    
}

+(id)UnArchiveData:(NSData *)data{
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

+(NSData *)ArchiveData:(id)obj{
    return [NSKeyedArchiver archivedDataWithRootObject:obj];
}

+(NSData *)getDataFromURL:(NSString *)urlString
{
    if (urlString.length == 0) {
        return nil;
    }
    
    NSString *fileName =[urlString lastPathComponent];
    
    if([FileHelper isExistWithPath:DocumentPathSource andName:fileName])
    {
        
        return [FileHelper dataWithPath:DocumentPathSource andName:fileName];
    }
    else
    {
        
        NSURL *url = [NSURL URLWithString:urlString];
        NSData *data =[NSData dataWithContentsOfURL:url];
        [FileHelper save:data withPath:DocumentPathSource andName:fileName];
        return data;
    }
}

+(void)preloadItemFromURL:(NSString*)urlString
{
    if (urlString.length == 0) {
        return;
    }
    
    NSString *fileName =[urlString lastPathComponent];
    
    if([FileHelper isExistWithPath:DocumentPathSource andName:fileName])
    {
      
        return;
    }
    else
    {
        
        NSURL *url = [NSURL URLWithString:urlString];
        [FileHelper save:[NSData dataWithContentsOfURL:url] withPath:DocumentPathSource andName:fileName];
        
    }

}


+(id)loadJsonTextFile:(NSString*)fileName
{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"txt"];
    NSError *error;
    NSString *json = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    
    if (error) {
        return nil;
    }
    NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
    
    id item = [NSJSONSerialization JSONObjectWithData:jsonData
                                                     options:NSJSONReadingMutableContainers
                                                       error:&error];
    
    if (error) {
        return nil;
    }
    
   return item;
}


+(BOOL)isBundleExist:(NSString *)fileName
{
    BOOL result;
    
    NSString *path;
    
    path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath],fileName];
    
    
    result = ([[NSFileManager defaultManager] fileExistsAtPath:path]);
    
    return result;
    
}
@end
