//
//  QuestionGroupInfo.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/30/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <Foundation/Foundation.h>

@class QuestionInfo;

@interface QuestionGroupInfo : NSObject
//"_id": "2417",
//"qgroup_th": "การถามเกี่ยวกับอุปนิสัยโดยทั่วๆไป",
//"qgroup_mm": "အေထြေထြ လူေနမွဳ အမူအက်င့္မ်ားႏွင့္ ပတ္သက္၍ ေမးျမန္းျခင္း",
//"order_item": "26"

@property (nonatomic,assign) NSInteger pID;
@property (nonatomic,strong) NSString *pTxtTh;
@property (nonatomic,strong) NSString *pTxtMM;
@property (nonatomic,assign) NSInteger pOrder;

@property (nonatomic,strong) NSArray *pQuestionArray;


+(NSArray*)initWithArray:(NSArray*)array;
+(id)initWithDictionary:(NSDictionary*)dic;


@end
