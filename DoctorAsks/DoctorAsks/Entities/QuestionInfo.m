//
//  QuestionInfo.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/30/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "QuestionInfo.h"
#import "AnswerInfo.h"

@implementation QuestionInfo


+(NSArray*)initWithArray:(NSArray*)array
{
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in array) {
        [temp addObject:[QuestionInfo initWithDictionary:dic]];
    }
    
    return [NSArray arrayWithArray:temp];
}
+(id)initWithDictionary:(NSDictionary*)dic{
    
    QuestionInfo *info = [[QuestionInfo alloc]init];
    
    info.pID = [dic intForKey:@"_id"];
    info.pGroupID = [dic intForKey:@"qgroup_id"];
    info.pTxtTh = [dic stringNotNullForKey:@"qlist_th"];
    info.pTxtMM = [dic stringNotNullForKey:@"qlist_mm"];
    info.pSoundTh = [dic stringNotNullForKey:@"qlist_th_sound"];
    info.pSoundMM = [dic stringNotNullForKey:@"qlist_mm_sound"];
    info.pOrder = [dic intForKey:@"order_item"];
    
    info.pSoundExist = [FileHelper isBundleExist:info.pSoundMM];
    
    return info;
}


//"_id": "1817",
//"qgroup_id": "2416",
//"qlist_th": "ขอทราบประวัติและพูดคุยกับท่าน",
//"qlist_mm": "ယခင္ကခံစားခဲ့ဘူးေသာ က်န္းမာေရးဆိုင္ရာျပသာနာမ်ားကို ေျပာျပေပးေစလိုပါတယ္",
//"qlist_th_sound": "b5c57b28-318f-4026-bd26-4b04eab24dac.mp3",
//"qlist_mm_sound": "b5c57b28-318f-4026-bd26-4b04eab24dac.mp3",
//"order_item": "1"
@end
