//
//  QuestionInfo.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/30/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AnswerInfo;

@interface QuestionInfo : NSObject


@property (nonatomic,assign) NSInteger pID;
@property (nonatomic,assign) NSInteger pGroupID;
@property (nonatomic,strong) NSString *pTxtTh;
@property (nonatomic,strong) NSString *pTxtMM;
@property (nonatomic,strong) NSString *pSoundTh;
@property (nonatomic,strong) NSString *pSoundMM;
@property (nonatomic,assign) NSInteger pOrder;
@property (nonatomic,assign) BOOL pSoundExist;

@property (nonatomic,strong) NSArray *pAnswerArray;


+(NSArray*)initWithArray:(NSArray*)array;
+(id)initWithDictionary:(NSDictionary*)dic;

@end

//"_id": "1817",
//"qgroup_id": "2416",
//"qlist_th": "ขอทราบประวัติและพูดคุยกับท่าน",
//"qlist_mm": "ယခင္ကခံစားခဲ့ဘူးေသာ က်န္းမာေရးဆိုင္ရာျပသာနာမ်ားကို ေျပာျပေပးေစလိုပါတယ္",
//"qlist_th_sound": "b5c57b28-318f-4026-bd26-4b04eab24dac.mp3",
//"qlist_mm_sound": "b5c57b28-318f-4026-bd26-4b04eab24dac.mp3",
//"order_item": "1"
