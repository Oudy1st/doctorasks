//
//  QALinkInfo.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/31/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QALinkInfo : NSObject


@property (nonatomic,assign) NSInteger pID;
@property (nonatomic,assign) NSInteger pQuestionID;
@property (nonatomic,assign) NSInteger pAnswerID;
@property (nonatomic,assign) NSInteger pOrder;

//"_id": "339",
//"q_id": "1977",
//"a_id": "356",
//"order_item": "8"


+(NSArray*)initWithArray:(NSArray*)array;
+(id)initWithDictionary:(NSDictionary*)dic;
@end
