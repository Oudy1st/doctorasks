//
//  AnswerInfo.h
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/30/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnswerInfo : NSObject


@property (nonatomic,assign) NSInteger pID;
@property (nonatomic,strong) NSString *pTxtTh;
@property (nonatomic,strong) NSString *pTxtMM;
@property (nonatomic,strong) NSString *pSoundTh;
@property (nonatomic,strong) NSString *pSoundMM;
@property (nonatomic,assign) NSInteger pOrder;
@property (nonatomic,assign) BOOL pSoundExist;



+(NSArray*)initWithArray:(NSArray*)array;
+(id)initWithDictionary:(NSDictionary*)dic;

@end




//"_id": "367",
//"alist_th": "ให้คนไข้ชี้ปฏิทิน",
//"alist_mm": "",
//"alist_th_sound": null,
//"alist_mm_sound": null,
//"order_item": "114"