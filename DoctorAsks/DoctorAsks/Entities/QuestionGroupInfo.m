//
//  QuestionGroupInfo.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/30/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "QuestionGroupInfo.h"
#import "QuestionInfo.h"

@implementation QuestionGroupInfo



+(NSArray*)initWithArray:(NSArray*)array
{
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in array) {
        [temp addObject:[QuestionGroupInfo initWithDictionary:dic]];
    }
    
    return [NSArray arrayWithArray:temp];
}
+(id)initWithDictionary:(NSDictionary*)dic{
    
    QuestionGroupInfo *info = [[QuestionGroupInfo alloc]init];
    
    
    info.pID = [dic intForKey:@"_id"];
    info.pTxtTh = [dic stringNotNullForKey:@"qgroup_th"];
    info.pTxtMM = [dic stringNotNullForKey:@"qgroup_mm"];
    info.pOrder = [dic intForKey:@"order_item"];
    
    
//    "_id": "2416",
//    "qgroup_th": "การถามประวัติเจ็บป่วย โดยทั่วๆ ไป",
//    "qgroup_mm": "အေထြေထြေရာဂါႏွင့္ပတ္သက္ေသာအေၾကာင္းအရာမ်ားအားေမးျမန္းျခင္း",
//    "order_item": "1"
    
    return info;
}


@end
