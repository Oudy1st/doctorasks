//
//  QALinkInfo.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/31/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "QALinkInfo.h"

@implementation QALinkInfo
+(NSArray*)initWithArray:(NSArray*)array
{
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in array) {
        [temp addObject:[QALinkInfo initWithDictionary:dic]];
    }
    
    return [NSArray arrayWithArray:temp];
}
+(id)initWithDictionary:(NSDictionary*)dic{
    
    QALinkInfo *info = [[QALinkInfo alloc]init];
    
    info.pID = [dic intForKey:@"_id"];
    info.pQuestionID = [dic intForKey:@"q_id"];
    info.pAnswerID = [dic intForKey:@"a_id"];
    info.pOrder = [dic intForKey:@"order_item"];
    return info;
}


//"_id": "339",
//"q_id": "1977",
//"a_id": "356",
//"order_item": "8"

@end
