//
//  AnswerInfo.m
//  DoctorAsks
//
//  Created by Detchat Boonpragob on 3/30/2558 BE.
//  Copyright (c) 2558 EGA. All rights reserved.
//

#import "AnswerInfo.h"

@implementation AnswerInfo


+(NSArray*)initWithArray:(NSArray*)array
{
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dic in array) {
        [temp addObject:[AnswerInfo initWithDictionary:dic]];
    }
    
    return [NSArray arrayWithArray:temp];
}
+(id)initWithDictionary:(NSDictionary*)dic{
    
    AnswerInfo *info = [[AnswerInfo alloc]init];
    
    info.pID = [dic intForKey:@"_id"];
    info.pTxtTh = [dic stringNotNullForKey:@"alist_th"];
    info.pTxtMM = [dic stringNotNullForKey:@"alist_mm"];
    info.pSoundTh = [dic stringNotNullForKey:@"alist_th_sound"];
    info.pSoundMM = [dic stringNotNullForKey:@"alist_mm_sound"];
    info.pOrder = [dic intForKey:@"order_item"];
    info.pSoundExist = [FileHelper isBundleExist:info.pSoundMM];
    
    return info;
}



//"_id": "367",
//"alist_th": "ให้คนไข้ชี้ปฏิทิน",
//"alist_mm": "",
//"alist_th_sound": null,
//"alist_mm_sound": null,
//"order_item": "114"

@end
